sharedContactsApp.directive('infiniteScroll', function($window, $document)
{
    return {
        link: function(scope, element, attr)
        {
          if(attr.isBody && attr.isBody == 'true')
          {
            $().ready(function()
            {
                $(window).scroll(function()
                {
                    var scrollPercent = ($(window).scrollTop() / ($(document).height() - $(window).height())) * 100;
                    if (scrollPercent >= 100)
                    {
                        scope.$apply(attr.infiniteScroll);
                    }
                })
            });
          }
          else
          {
            element.bind('scroll', function()
            {
              if (element['0'].offsetHeight + element['0'].scrollTop >= element['0'].scrollHeight)
              {
                scope.$apply(attr.infiniteScroll);
              }
            });
          }
        }
    };
});