
angular.module('Services').factory('SharedContactsService', function($resource)
{
    return $resource('/contacts', {},
    {
        get_all: { method: 'POST', params:{ action: 'get_all' }, isArray: false },
        search_contacts: { method: 'POST', params:{ action: 'search_contacts' }, isArray: false },
        get_contacts_number: { method: 'POST', params:{ action: 'get_contacts_number' }, isArray: false },
        insert_or_update: { method: 'POST', params:{ action: 'insert_or_update' }, isArray: false },
        delete_contact: { method: 'POST', params:{ action: 'delete_contacts' }, isArray: false },
        get_app_users: { method: 'POST', params:{ action: 'get_app_users' }, isArray: true },
        delete_app_user: { method: 'POST', params:{ action: 'delete_app_user' }, isArray: false },
        update_app_user: { method: 'POST', params:{ action: 'update_app_user' }, isArray: false }
    });
});