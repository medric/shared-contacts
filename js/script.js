/**
 * Created by medric on 3/20/14.
 */

$(document).ready(function(){
    $('.message-info').show();
    $('#back-to-list').hide();
    $('#search-table-wrapper').hide();
    $('#menu-aside').hide();

    $('#export-contacts').tooltip();
    /**
     * Formulaire ajout/modif selected_contact
     *
     * */
     $('#contact-form').hide();
     $('#btn-delete-contacts').hide();

    /**
     *
     * Menu aside, gestion active
     */
    $('.list-group a').on('click', function(){
        $(this).parent().find('.active').removeClass('active');
        $(this).addClass('active');
    });

    /**
     *
     * Changement de fond de la ligne quand elle est sélectionnée
     */
    $('body').on('click', 'input.cb-selected-contact', function(){
        if($(this).is(':checked')){
            $(this).closest('.contact-item').css('background','#FFF5D7');
        }
        else{
            $(this).closest('.contact-item').css('background','#FFF')
        }
    });

    // Message info
    remove_message();
 });

/* Sauvegarde du domaine pour le super-admin*/
function set_admin_domain(domain){
    sessionStorage.setItem('admin_domain', domain)
}

function get_admin_domain(){
    return sessionStorage.getItem('admin_domain');
}

/* Sauvegarde message de reponse serveur après une requête serveur (ajout, modif, suppression) */
function set_message_alert(message){
    sessionStorage.setItem('message_alert', message);
}

function get_message_alert(){
    return sessionStorage.getItem('message_alert');
}

/* Suppression message info*/
function remove_message(){
    setTimeout(function(){
        $('.message-info').fadeOut();
        set_message_alert('');
    }, 4000);
}