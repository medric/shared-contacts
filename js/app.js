var sharedContactsApp = angular.module('sharedContactsApp', [
        'ngResource',
        'Filters',
        'Services',
        'checklist-model'
    ]).
	config(
        function($interpolateProvider){
            $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
        }
    );

angular.module('Filters', []);
angular.module('Services', ['ngResource'])