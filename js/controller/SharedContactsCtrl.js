/**
 * Created by medric on 3/21/14.
 */

// TO DO : GESTION ERREURS (Exceptions ...)
sharedContactsApp.controller('SharedContactsCtrl', function($scope, SharedContactsService){
    // Users
    $scope.users = [];
    // Init
    $scope.contacts = [];
    $scope.search_key = ''
    $scope.searched_contacts = [];
    $scope.to_delete = [];
    $scope.contacts_total = null;
    $scope.selected_contact = null;
    $scope.action = 'view_all'

    // Message info
    $scope.message = get_message_alert();
    console.log($scope.message);

    // Super-admin : domain par défaut
    $scope.domain = get_admin_domain(); // On regarde s'il est sauvegardé dans le localstorage

    if($scope.domain){
        $scope.current_domain = $('#' + $scope.domain).html();
    }

    $('.message-info').show();

    // Formulaire ajout/modif contacts
    $scope.contact_form = {
        url: 'templates/contact_form.html'
    };

    // Paramètres pour la pagination des contacts
    $scope.offset = 0;
    $scope.limit = 25;

    $scope.select_domain = function($obj){
        $('#on-load').show();
        $scope.reset();
        value = $obj.toElement.attributes[3].value;
        $scope.current_domain = $('#' + value).html();
        // Sauvagarde dans le localstorage
        set_admin_domain(value);
        // Reset
        $scope.offset = 0;
        $scope.limit = 25;
        $scope.domain = value;
        location.reload();
    }

    $scope.reset = function(){
        $scope.contacts = [];
        $scope.searched_contacts = [];
        $scope.to_delete = [];

        //Affichage
        $('#btn-delete-contacts').hide();
        $('#manage-contact').hide();
        $('#search-table-wrapper').hide();
        $('#back-to-list').hide();
        $('.message-info').hide();
    }

    $scope.paginate = function(){
       var options = {
            offset: $scope.offset
            ,limit: $scope.limit
       };

       if($scope.domain){
           options.domain = $scope.domain;
       }

       SharedContactsService.get_all(options, function(response){
            if(response.data){
                $('#btn-delete-contacts').show();

                // Ajout des contacts dans le scope
                response.data.forEach(function(entry) {
                    $scope.contacts.push(entry);
                });
            }

            // Affichage du menu, disparition du loader
            $('#menu-aside').fadeIn('slow');
            $('#on-load').hide();
            $scope.offset = response.offset;
            $scope.contacts_total = response.total_count;
       });
    }

    $scope.search_contacts = function(){
        $scope.action = "view_search"
        $scope.search_key = $('#contact-search').val();
        $('#contacts-table-wrapper').hide();
        $('#btn-delete-contacts').hide();
        $('#manage-contact').hide();
        $scope.info = '';

        // Loader
        $('#on-load').show();

        // Reset
        $scope.searched_contacts = [];
        $scope.search_offset = 0;
        $scope.search_limit = 25;

        $scope.paginate_search();
    }

    $scope.paginate_search = function(){
        var options = {
            offset: $scope.search_offset
            ,limit: $scope.search_limit
            ,value: $scope.search_key
        };

        if($scope.domain){
           options.domain = $scope.domain;
        }

        SharedContactsService.search_contacts(options, function(response){
            $('#search-table-wrapper').show();
            $('#back-to-list').show();

            if(response.data.length > 0){
                $scope.info = null;
                $('#btn-delete-contacts').show();

                response.data.forEach(function(entry) {
                    $scope.searched_contacts.push(entry);
                });

                $('#on-load').hide();
                $scope.search_offset = response.offset
            }
            else{
                $('#on-load').hide();
                $scope.info = 'No result';
            }
        });
    }

    $scope.load_form = function(contact, $obj){
        $scope.reset_to_delete_list();
        $scope.selected_contact = contact;
        if($obj){
            $elem = $($obj.target);
            $scope.action = $elem.closest('.contact-item').attr('data-action');
        }
        $('#btn-delete-contacts').hide();
        $('#manage-contact').show();
        $('#contacts-table-wrapper').hide()
        $('#search-table-wrapper').hide();
        $('#back-to-list').show();
    }

    $scope.insert_or_update = function(){
        var mail = $('#contact-email').val();
        var lastName = $('#contact-name').val();
        var firstName = $('#contact-firstname').val();
        var address = {
            street: $('#contact-address-street').val(),
            city: $('#contact-address-city').val(),
            postcode: $('#contact-address-postcode').val(),
            region: $('#contact-address-region').val(),
            country: $('#contact-address-country').val()
        }
        var mail = $('#contact-email').val();
        var organization  = $('#contact-organization').val();
        var organizationLabel  = $('#contact-organization-label').val();
        var phone = $('#contact-phone').val();

        $('#on-load').show();
        $('#back-to-list').hide();

        if(checkMail(mail)){
            $('#btn-form-validate').button('loading');

            var contact = {
                mail: mail
                ,lastName: lastName
                ,firstName: firstName
                ,address: address
                ,mail: mail
                ,organization: organization
                ,organizationLabel: organizationLabel
                ,phoneNumber: phone
            }

            var options = {
                contact: contact
            };

            if($scope.domain){
                options.domain = $scope.domain;
            }

            SharedContactsService.insert_or_update(options, function(response){
                if(response.message == 'success'){
                    set_message_alert('Contacts up to date');
                    location.reload();
                }
                else{
                    $scope.message = 'An error occured';
                    $('#contact-form').reset();
                    $('.btn').reset();
                    location.reload();
                }
            });
        }
        else{
            $scope.message = 'Email not valid'
            $('#back-to-list').show();
            $('#on-load').hide();
        }
    };

    $scope.back_to_list = function(){
        switch($scope.action){
            case 'view_all':
                $('#btn-delete-contacts').show();
                $('#contacts-table-wrapper').show();
                $('#search-table-wrapper').hide();
                $('#back-to-list').hide();
                $('#manage-contact').hide();
                $('.list-group-item').removeClass('active');
            break;
            case 'view_search':
                $('#btn-delete-contacts').show();
                $('#contacts-table-wrapper').hide();
                $('#search-table-wrapper').show();
                $('#back-to-list').show();
                $('#manage-contact').hide();
                $('.list-group-item').removeClass('active');
                $scope.action = 'view_all';
            break;
        }
        $scope.reset_to_delete_list();
    }

    $scope.reset_to_delete_list = function(){
        $scope.to_delete = [];
        $("input.cb-selected-contact").attr('checked', false);
        $("input.cb-selected-contact").css('background','#FFF');
    }

    $scope.delete_contacts = function(){
        // Loader
        $('#on-load').show();
        $('#back-to-list').hide();

        if($scope.selected_contact){
            $scope.to_delete.push($scope.selected_contact.contactId);
        }

        var options = {
            contacts: $scope.to_delete
        };

        if($scope.domain){
           options.domain = $scope.domain;
        }

        if($scope.to_delete != []){
            SharedContactsService.delete_contact(options, function(response){
                if(response.message == 'success'){
                    set_message_alert('Contact(s) deleted');
                    location.reload();
                }
                else{
                    $('#on-load').hide();
                    $('#back-to-list').show();
                    $scope.message = 'An error occured';
                }
                $('.message-info').show();
                // Suppression message alert
                remove_message();
                $scope.reset_to_delete_list();
            });
        }
    }

    // AppUsers
    $scope.get_app_users = function(){
        var options = {}

        if($scope.domain){
           options.domain = $scope.domain;
        }

        SharedContactsService.get_app_users(options, function(response){
            response.forEach(function(entry){
                $scope.users.push(entry);
            });
        });
    }

    $scope.delete_user = function(id){
        var options = {
            id: id
        }

        if($scope.domain){
            options.domain = $scope.domain
        }

        SharedContactsService.delete_app_user(options, function(response){
            if(response.message == 'success'){
                location.reload();
            }
            else{
                $('#on-load').hide();
                $('#back-to-list').show();
                $scope.message = 'An error occured';
            }
            $('.message-info').show();
            // Suppression message alert
            remove_message();
        });
    }

    $scope.update_user = function($obj, id){
        var $self = $($obj.toElement)
        var role = $self.closest('tr').find('select').val();

        var options = {
            id: id,
            role: role
        }

        SharedContactsService.update_app_user(options, function(response){
            if(response.message == 'success'){
                location.reload();
            }
            else{
                $('#on-load').hide();
                $('#back-to-list').show();
                $scope.message = 'An error occured';
            }
            $('.message-info').show();
            // Suppression message alert
            remove_message();
        });
    }

    // Appelé au chargement de la page
    $scope.paginate();
});
