var Request = {

    options: {
        url: ''
        ,data: ''
    }

    ,response: ''

    ,init: function(params){
        $.extend(this.options, params);
    }

    ,send: function(){
        var data = null;

        var self = this;

        $.ajax({
            async: false
            ,url : self.options.url
            ,type: 'POST'
            ,data: self.options.data
            ,dataType: 'json'

        }).done(function(response){
            self.response = response;
        });
    }
}