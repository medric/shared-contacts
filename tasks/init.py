__author__ = 'medric'

import cgi
import webapp2

from models.app_user import AppUser


class Init(webapp2.RequestHandler):
    def get(self):
        mail = cgi.escape(self.request.get('mail'))
        mail = str(mail)

        if mail:
            if AppUser.super_admin_exists(mail):
                self.redirect('/')
            else:
                user = AppUser(id=AppUser.encode_md5_hash(mail))
                user.email = mail
                user.role = 'super_admin'
                user.domain = ''

                user.insert_user()

        self.redirect('/')