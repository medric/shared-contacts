from api_manager import ApiManager

__author__ = 'medric'

from request import Request
from models.shared_contact import SharedContact
from models.service_account import ServiceAccount
from models.address import Address
import webapp2
import json
import cgi
from google.appengine.ext import ndb
from google.appengine.api import taskqueue
from utils.encode import Encode
import logging


class Population(webapp2.RequestHandler):
    def get(self):
        # On vide tous les contacts du datastore
        # ndb.delete_multi(
        #     SharedContact.query().fetch(keys_only=True)
        # )
        # Appel du process pour chaque domaine
        for domain in ServiceAccount.get_allowed_domains():
            taskqueue.add(url='/load', params={'domain': domain['name']})


class DownloadContacts(webapp2.RequestHandler):
    def post(self):
        next_page = 1
        max_results = 700

        domain = cgi.escape(self.request.get('domain'))
        service = ApiManager.get_api_service(domain)

        if service:
            feed_uri = service.GetFeedUri(
                contact_list=domain,
                projection='full'
            )

            contacts_tmp = []
            # Tant qu'il y a des contacts a recuperer
            while next_page:
                try:
                    data = Request.retrieve_shared_contacts(next_page, max_results, service, feed_uri)
                    if data['data']:
                        # Pour chaque entree, creation d'une entite dans le datastore
                        for entry in data['data']:
                            entry = json.loads(entry)
                            try:
                                mail = entry['email'][0]['address']
                                if mail:
                                    try:
                                        lastName = entry['name']['family_name']['text']
                                    except Exception:
                                        lastName = ''
                                    try:
                                        firstName = entry['name']['given_name']['text']
                                    except Exception:
                                        firstName = ''
                                    try:
                                        organization = entry['organization']['title']['text']
                                    except Exception:
                                        organization = ''
                                    try:
                                        phoneNumber = entry['phone_number'][0]['text']
                                    except Exception:
                                        phoneNumber = ''

                                    try:
                                        address = Address(
                                            street=entry['structured_postal_address'][0]['street']['text'],
                                            city=entry['structured_postal_address'][0]['city']['text'],
                                            postcode=entry['structured_postal_address'][0]['postcode']['text'],
                                            region=entry['structured_postal_address'][0]['region']['text'],
                                            country=entry['structured_postal_address'][0]['country']['text']
                                        )
                                    except Exception as e:
                                        address = None

                                    # KEYNAME = md5 du domaine + id -> unique
                                    id = Request.get_contact_id(entry)
                                    contact = SharedContact(id=Encode.encode_md5_hash(domain + id))
                                    contact.contactId = id
                                    contact.mail = mail
                                    contact.firstName = firstName
                                    contact.lastName = lastName
                                    contact.organization = organization
                                    contact.phoneNumber = phoneNumber
                                    if address:
                                        contact.address = address
                                    contact.domain = domain

                                    #ajout du contact dans une liste
                                    contacts_tmp.append(contact)
                                else:
                                    continue
                            except Exception as e:
                                continue
                        next_page = data['next_page']
                    else:
                        next_page = None
                except Exception as e:
                    logging.log(e.message)
            #insert multi - contacts
            to_add = DownloadContacts.to_add(contacts_tmp, domain)
            logging.debug('to_add =' + str(len(to_add)))
            try:
                if to_add:
                    ndb.put_multi(to_add)
            except Exception as e:
                logging.error(e.message)

    @staticmethod
    def to_add(tmp_lists, domain):
        contacts_lists = SharedContact.get_full(domain)
        out = []
        for entity in tmp_lists:
            o = filter(lambda x: x.contactId == entity.contactId and x.domain == entity.domain, contacts_lists)
            if o:
                #le contact a ete modifie sur les serveurs Google
                if not o[0].compare_other(entity):
                    out.append(entity)
            #le contact n'existe pas encore
            else:
                out.append(entity)
        return out