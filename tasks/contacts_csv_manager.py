from datetime import date

__author__ = 'medric'

import csv
import StringIO
from request import Request
from google.appengine.api import mail
from server.report import Report
import globales
import datetime
import logging
from models.shared_contact import SharedContact
from models.address import Address

# Actions
ACTION_ADD = 'add'
ACTION_UPDATE = 'update'
ACTION_DELETE = 'delete'


# Enum pour les colonnes du fichier CSV (import de contacts)
class Columns:
    ACTION = 0
    ID = 1
    MAIL = 2
    LASTNAME = 3
    FIRSTNAME = 4
    ORGANIZATION = 5
    LABEL = 6
    PHONENUMBER = 7
    STREET = 8
    CITY = 9
    POSTCODE = 10
    REGION = 11
    COUNTRY = 12
    LINK = 13


class contactsCSVManager():
    @staticmethod
    def contacts_csv_manager(service, to, feed_uri, domain, data):
        reader = csv.reader(StringIO.StringIO(data))
        # report
        report = []

        i = 0
        try:
            for row in reader:
                try:
                    if i > 0:
                        action = row[Columns.ACTION]
                        action = action.lower()
                        if not action == '':
                            contact = SharedContact()
                            contact.contactId = row[Columns.ID]
                            contact.mail = row[Columns.MAIL]
                            contact.lastName = row[Columns.LASTNAME]
                            contact.firstName = row[Columns.FIRSTNAME]
                            contact.organization = row[Columns.ORGANIZATION]
                            contact.organizationLabel = row[Columns.LABEL]
                            contact.phoneNumber = row[Columns.PHONENUMBER]
                            address = Address(
                                street=row[Columns.STREET],
                                city=row[Columns.CITY],
                                postcode=row[Columns.POSTCODE],
                                region=row[Columns.REGION],
                                country=row[Columns.COUNTRY]
                            )
                            if not address.is_empty():
                                contact.address = address
                            if len(row) > 14:
                                raise Exception('Format error')

                            if mail.is_email_valid(contact.mail):
                                if action == ACTION_ADD:
                                    contactsCSVManager.add(service, feed_uri, contact)
                                if action == ACTION_UPDATE:
                                    contactsCSVManager.update(service, feed_uri, contact)
                                elif action == ACTION_DELETE:
                                    contactsCSVManager.delete(contact.contactId, service, feed_uri)
                            else:
                                # report erreur
                                report.append(contact.mail)
                    i += 1
                except Exception as e:
                    # report erreur
                    if contact.mail:
                        report.append(contact.mail)
                    elif contact:
                        report.append('format error')
                    continue
        except csv.Error as e:
            report.append(e.message)
            logging.error(e.message)
        except IndexError:
            report.append(e.message)
        finally:
            if len(report) > 0:
                Report.send_mail_error(globales.MAIL_SENDER, to, 'report upload shared-contact', report,
                                        datetime.date.today().strftime('%d %b %Y'))
            else:
                Report.send_mail(globales.MAIL_SENDER, to, 'report upload shared-contact',
                                 datetime.date.today().strftime('%d %b %Y'))

    @staticmethod
    def add(service, feed_uri, contact):
        try:
            if contact.contactId == '':
                service.create_contact(contact.to_contact_entry(), feed_uri)
        except Exception as e:
            logging.error(e.message)

    @staticmethod
    def update(service, feed_uri, contact):
        try:
            if not contact.contactId:
                entry = Request.get_contact_by_id(contact.contactId, service, feed_uri)
                if entry:
                    contact.update_shared_contact(entry, service)

        except Exception as e:
            logging.error(e.message)

    @staticmethod
    def delete(id, service, feed_uri):
        try:
            entry = Request.get_contact_by_id(id, service, feed_uri)
            service.Delete(entry)
        except Exception as e:
            logging.error(e.message)
