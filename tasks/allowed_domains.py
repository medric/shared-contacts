__author__ = 'medric'

from user_info import UserInfo
from models.service_account import ServiceAccount
from google.appengine.api import users
from utils.encode import Encode

import webapp2

DICT_DOMAINS = [
    {
        'domain': 'gtraining.eu',
        'email_admin': 'melvin@gtraining.eu',
        'pem_key': file('pemkey/gtraining.pem', 'rb').read(),
        'email_address': '11100742372-9cj7l3str354gti5nfhsoqclo2s7ln1l@developer.gserviceaccount.com'
    },
    # {
    #     'domain': 'manitou-group.com',
    #     'email_admin': 'gpadmin@manitou-group.com',
    #     'pem_key': file('pemkey/manitou.pem', 'rb').read(),
    #     'email_address': '186491246510-ifsfvvod9b4r7fkmsctjs15ah0b1brfn@developer.gserviceaccount.com'
    # }
]


class InitServiceAccount(webapp2.RequestHandler):
    def get(self):
        if UserInfo.current_user():
            if UserInfo.current_user().is_super_admin():
                for entry in DICT_DOMAINS:
                    service_account = ServiceAccount(
                        id=Encode.encode_md5_hash(entry['domain']),
                        domain=entry['domain'],
                        email_admin=entry['email_admin'],
                        pem_key=entry['pem_key'],
                        email_address=entry['email_address']
                    )
                    service_account.insert_account()
                self.redirect('/')
            else:
                self.response.write('<h1>FORBIDDEN</h1>')
        else:
            greeting = users.create_login_url('/')
            self.redirect(greeting)