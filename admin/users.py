__author__ = 'medric'

__author__ = 'medric'

import cgi
import time

import webapp2

import globales
from models.app_user import AppUser
from user_info import UserInfo

roles = ['user', 'admin']


class Users(webapp2.RequestHandler):
    def get(self):
        try:
            if UserInfo.is_current_user_super_admin() or UserInfo.is_current_user_admin():
                greetings = UserInfo.greetings()
                self.response.write(globales.INDEX.render(user=greetings))
                self.response.write(globales.APP_USERS_TEMPLATE.render(user=greetings))
                self.response.write(globales.FOOTER.render())
            else:
                self.response.out.write(globales.ACCESS_ERROR.render())
        except Exception as e:
            self.response.out.write(globales.ACCESS_ERROR.render())