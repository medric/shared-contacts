__author__ = 'medric'

import cgi
import time

import webapp2

import globales
from models.app_user import AppUser
from user_info import UserInfo


class UserAdd(webapp2.RequestHandler):
    def get(self):
        try:
            mail = cgi.escape(self.request.get('user-email'))
            role = cgi.escape(self.request.get('role'))

            try:
                domain = cgi.escape(self.request.get('domain'))
            except Exception as e:
                domain = UserInfo.current_user().domain

            if not domain or domain == '':
                raise Exception('no domain')

            if AppUser.get_user(mail):
                raise Exception('This user already exists')

            if role in globales.USER_ROLES:
                user = AppUser(
                    email=mail,
                    role=role,
                    domain=domain
                )
                user.insert_user()
            else:
                raise Exception('This role does not exist')

            self.redirect('/users')
        except Exception as e:
            self.redirect('/users')
