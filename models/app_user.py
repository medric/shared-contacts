__author__ = 'medric'

import hashlib
from google.appengine.ext import ndb


class AppUser(ndb.Model):
    email = ndb.StringProperty()
    role = ndb.StringProperty()
    domain = ndb.StringProperty()

    def insert_user(self):
        self.put()

    def delete_user(self):
        self.key.delete()

    def is_allowed(self, domain):
        return self.domain == domain

    def is_admin(self):
        return self.role == 'admin'

    def is_super_admin(self):
        return self.role == 'super_admin'

    @staticmethod
    def encode_md5_hash(string):
        m = hashlib.md5()
        m.update(string.encode('utf-8'))
        return m.hexdigest()

    @staticmethod
    def get_user(mail):
        q = AppUser.query(
            AppUser.email == mail
        )
        res = q.fetch(1)
        if not res:
            return None
        else:
            return res[0]

    @staticmethod
    def get_user_by_id(id):
        res = AppUser.get_by_id(id)
        if not res:
            return None
        else:
            return res

    @staticmethod
    def get_users_by_domain(domain):
        q = AppUser.query(AppUser.domain == domain)

        users = []

        for p in q.fetch():
            if p.role != 'super_admin':
                user = {
                   'email': p.email,
                   'role': p.role,
                   'id': p.key.id()
                }
                users.append(user)

        return users

    @staticmethod
    def super_admin_exists(mail):
        q = AppUser.query(AppUser.role == 'super-admin')
        res = q.fetch(1)
        if not []:
            return None
        else:
            return res[0]

    @staticmethod
    def user_exists(mail):
        if AppUser.get_user(mail):
            return True
        else:
            return False

    @staticmethod
    def get_token_by_mail(email):

        q = AppUser.query(AppUser.email == email)

        token = None

        for p in q.fetch(1):
            token = p.token

        return token

    @staticmethod
    def get_domain_from_email(email):
        domain = str(email).split('@')
        try:
            return domain[1]
        except:
            return domain[0]

