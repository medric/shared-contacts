__author__ = 'medric'

from google.appengine.ext import ndb
import globales


class ServiceAccount(ndb.Model):
    domain = ndb.StringProperty()
    email_admin = ndb.StringProperty()
    pem_key = ndb.TextProperty()
    email_address = ndb.StringProperty()

    def insert_account(self):
        self.put()

    def delete_account(self):
        self.key.delete()

    def get_domain(self):
        return self.domain

    def get_email_admin(self):
        return self.email_admin

    def get_pem_key(self):
        return self.pem_key

    def get_email_address(self):
        return self.email_address

    @staticmethod
    def get_by_domain(domain):
        q = ServiceAccount.query(ServiceAccount.domain == domain)
        res = q.fetch(1)
        if not res:
            return None
        else:
            return res[0]

    @staticmethod
    def get_allowed_domains():
        domains = []
        q = ServiceAccount.query(ServiceAccount.domain != 'gpartner.eu')

        for p in q.fetch():
            domains.append({
                'name': p.domain,
                'value': p.key.id()
            })

        return domains

    @staticmethod
    def get_domain_name(id):
        data = ServiceAccount.get_by_id(id)
        if not data:
            return None
        else:
            return data.domain