__author__ = 'Medric'

from google.appengine.ext import ndb

import gdata
import gdata.contacts.client
import gdata.contacts.data
import gdata.gauth
from utils.jsonEncode import ModelEncoder
import logging
import webapp2
from user_info import UserInfo
from address import Address


# SharedContact model
class SharedContact(ndb.Model):
    domain = ndb.StringProperty()
    contactId = ndb.StringProperty()
    mail = ndb.StringProperty()
    lastName = ndb.StringProperty()
    firstName = ndb.StringProperty()
    organization = ndb.StringProperty()
    organizationLabel = ndb.StringProperty()
    phoneNumber = ndb.StringProperty()
    address = ndb.StructuredProperty(Address, repeated=False)

    def compare_other(self, other):
        return self == other

    def save(self):
        # Sauvegarde du modele dans le datastore
        self.put()

    # Encode l'objet courant au format JSON
    def to_json(self):
        return ModelEncoder().encode(self)

    def delete(self):
        self.key.delete()

    def to_contact_entry(self):
        #Cree une nouvelle entree (selected_contact Google)
        new_contact = gdata.contacts.data.ContactEntry()

        new_contact.email.append(
            gdata.data.Email(
                address=self.mail,
                primary='true',
                rel=gdata.data.WORK_REL
            )
        )

        if self.lastName:
            new_contact.name = gdata.data.Name(
                family_name=gdata.data.FamilyName(text=self.lastName)
            )

        if self.firstName:
            new_contact.name = gdata.data.Name(
                given_name=gdata.data.GivenName(text=self.firstName)
            )

        if self.organization:
            new_contact.organization = gdata.data.Organization(
                label=self.organizationLabel,
                primary='true',
                name=gdata.data.OrgName(text=self.organization),
                title=gdata.data.OrgTitle(text=self.organization)
            )

        if self.phoneNumber:
            new_contact.phone_number.append(
                gdata.data.PhoneNumber(
                    rel=gdata.contacts.REL_WORK,
                    text=self.phoneNumber
                )
            )

        if self.address:
            #set contacts postal address.
            new_contact.structured_postal_address.append(gdata.data.StructuredPostalAddress(
                rel=gdata.data.WORK_REL, primary='true',
                street=gdata.data.Street(text=self.address.street),
                city=gdata.data.City(text=self.address.city),
                region=gdata.data.Region(self.address.region),
                postcode=gdata.data.Postcode(text=self.address.postcode),
                country=gdata.data.Country(text=self.address.country)
            ))

        return new_contact

    def update_shared_contact(self, entry, service):
        try:
            entry.email = gdata.data.Email(
                address=self.mail,
                primary='true',
                rel=gdata.data.WORK_REL
            )
            entry.organization = gdata.data.Organization(
                label=self.organizationLabel,
                primary='true',
                name=gdata.data.OrgName(text=self.organization),
                title=gdata.data.OrgTitle(text=self.organization)
            )
            entry.phone_number = gdata.data.PhoneNumber(
                rel=gdata.contacts.REL,
                text=self.phoneNumber
            )
            entry.name = gdata.data.Name(
                family_name=gdata.data.FamilyName(text=self.lastName),
                given_name=gdata.data.GivenName(text=self.firstName),
                full_name=gdata.data.FullName(text=self.lastName + ' ' + self.firstName)
            )

            entry.structuredPostalAddress.append(gdata.data.StructuredPostalAddress(
                rel=gdata.data.WORK_REL, primary='true',
                street=gdata.data.Street(text=self.address.street),
                city=gdata.data.City(text=self.address.city),
                region=gdata.data.Region(self.address.region),
                postcode=gdata.data.Postcode(text=self.address.postcode),
                country=gdata.data.Country(text=self.address.country)
            ))

            service.update(entry)
        except Exception as e:
            logging.error(e.message)

    @staticmethod
    def get_contacts_paginate(offset, limit, search_domain):
        try:
            res = []

            q = SharedContact.query(
                SharedContact.domain == search_domain
            )

            contacts, cursor, more = q.fetch_page(limit, offset=offset)

            for contact in contacts:
                res.append(contact)

            if more:
                outcome = {
                    'data': res,
                    'offset': offset + limit + 1,
                    'total_count': SharedContact.get_contacts_number(search_domain)
                }

                return outcome
            else:
                outcome = {
                    'data': res,
                    'total_count': SharedContact.get_contacts_number(search_domain)
                }

            return outcome
        except Exception as e:
            raise Exception(e)

    @staticmethod
    def get_full(domain):
        try:
            data = SharedContact.query(SharedContact.domain == domain).fetch()
            return data
        except Exception as e:
            logging.log(e.message)

    @staticmethod
    def get_all_contacts(offset, limit):
        try:
            res = []
            q = SharedContact.query()
            contacts, cursor, more = q.fetch_page(limit, offset=offset)

            for contact in contacts:
                res.append(contact)

            if more:
                outcome = {
                    'data': res,
                    'offset': offset + limit + 1,
                    'total_count': SharedContact.get_contacts_number()
                }

                return outcome
            else:
                outcome = {
                    'data': res,
                    'total_count': SharedContact.get_contacts_number()
                }

            return outcome
        except Exception as e:
            raise Exception(e)

    @staticmethod
    def search_contacts(offset, limit, value, search_domain):
        try:
            res = []

            more = False

            # Requete sur mail, nom, et prenom
            q = SharedContact.query(
                ndb.OR(
                    SharedContact.mail == value,
                    SharedContact.firstName == value,
                    SharedContact.lastName == value,
                    SharedContact.mail == value.lower(),
                    SharedContact.firstName == value.lower(),
                    SharedContact.lastName == value.lower(),
                    SharedContact.mail == value.title(),
                    SharedContact.firstName == value.title(),
                    SharedContact.lastName == value.title()
                ),
                ndb.AND(SharedContact.domain == search_domain)
            ).order(SharedContact._key)

            contacts, more, cursor = q.fetch_page(limit, offset=offset)

            for contact in contacts:
                res.append(contact)

            if more:
                outcome = {
                    'data': res,
                    'offset': offset + limit + 1
                }

                return outcome
            else:
                outcome = {
                    'data': res,
                }

                return outcome
        except Exception as e:
            raise Exception(e)

    @staticmethod
    def get_contact_by_mail(mail, search_domain):
        try:
            model = SharedContact.query(
                ndb.AND(
                    SharedContact.mail == mail,
                    SharedContact.domain == search_domain
                )
            )

            res = model.fetch(1)

            if res is not []:
                return res[0]
            else:
                return None
        except Exception as e:
            return None

    @staticmethod
    def get_contact_by_id(contact_id, domain):
        try:
            model = SharedContact.query(
                SharedContact.contactId == contact_id,
                SharedContact.domain == domain
            )

            res = model.fetch(1)

            if res != []:
                return res[0]
            else:
                return None
        except Exception as e:
            return None


    @staticmethod
    def get_contacts_number(domain):
        try:
            return SharedContact.query(SharedContact.domain == domain).count()
        except Exception as e:
            return None


class DeleteAll(webapp2.RequestHandler):
    def get(self):
        if UserInfo.is_current_user_super_admin():
            ndb.delete_multi(
                SharedContact.query().fetch(keys_only=True)
            )
        self.redirect('/')