__author__ = 'medric'
from google.appengine.ext import ndb
from google.appengine.ext.db import BadValueError


class Address(ndb.Model):
    street = ndb.StringProperty()
    city = ndb.StringProperty()
    region = ndb.StringProperty()
    postcode = ndb.StringProperty()
    country = ndb.StringProperty()

    # def __init__(self, street='', city='', region='', postcode='', country=''):
    #     self.street = street
    #     self.city = city
    #     self.region = region
    #     self.postcode = postcode
    #     self.country = country

    def is_empty(self):
        return self.street == '' and self.city == '' and self.region == '' \
                and self.postcode == '' and self.country == ''

