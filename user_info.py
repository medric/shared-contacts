__author__ = 'medric'

from google.appengine.api import users
from models.app_user import AppUser
from models.service_account import ServiceAccount


class UserInfo:

    @staticmethod
    def current_user():
        # Retourne un objet AppUser correspondant a l'utilisateur courant
        user = users.get_current_user()
        try:
            current_user = AppUser.get_user(user.email())
        except Exception:
            current_user = None
        return current_user

    @staticmethod
    def greetings():
        user = {
            'mail': UserInfo.current_user().email,
            'logout': users.create_logout_url('/'),
            'super_admin': UserInfo.is_current_user_super_admin(),
            'admin': UserInfo.is_current_user_admin(),
            'domain': UserInfo.current_user().domain
        }

        if UserInfo.is_current_user_super_admin():
            user['domains'] = ServiceAccount.get_allowed_domains()

        return user

    @staticmethod
    def is_current_user_admin():
        return UserInfo.current_user().is_admin()

    @staticmethod
    def is_current_user_super_admin():
        return UserInfo.current_user().is_super_admin()

