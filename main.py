#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
from google.appengine.api import users

import globales


# handlers
from handlers.main import Main
from handlers.upload import Upload
from handlers.service_account_handler import ServiceAccountHandler
from handlers.export_handler import ExportHandler
from handlers.export_handler import Export

from tasks.population import Population
from tasks.population import DownloadContacts
from server.blob_server import UploadHandler
# from server.blob_server import DoUpload
from user_info import UserInfo
from admin.users import Users
from admin.user_add import UserAdd
from models.shared_contact import DeleteAll
# Initialisation / parametrage
from tasks.init import Init
from tasks.allowed_domains import InitServiceAccount


class MainHandler(webapp2.RequestHandler):
    def get(self):
        if UserInfo.current_user():
            greetings = UserInfo.greetings()
            self.response.write(globales.INDEX.render(user=greetings))
            self.response.write(globales.CONTENT.render(user=greetings))
            self.response.write(globales.FOOTER.render(user=greetings))
        else:
            self.response.out.write(globales.LOGIN_ERROR_TEMPLATE.render(login=users.create_login_url('/')))

app = webapp2.WSGIApplication([
    ('/init', Init),
    ('/service-account/init', InitServiceAccount),
    ('/upload', Upload),
    ('/upload-blob-csv', UploadHandler),
    # ('/do-upload', DoUpload),
    ('/users', Users),
    ('/users/add', UserAdd),
    ('/tasks/populate', Population),
    ('/load', DownloadContacts),
    ('/service-account', ServiceAccountHandler),
    ('/contacts', Main),
    ('/export', ExportHandler),
    ('/export/run', Export),
    ('/', MainHandler),
    ('/shared-contacts/delete', DeleteAll),

], debug=True)
