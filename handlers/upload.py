from google.appengine.api.oauth.oauth_api import is_current_user_admin

__author__ = 'medric'

import webapp2
import cgi
from user_info import UserInfo
import globales
from google.appengine.ext import blobstore


class Upload(webapp2.RequestHandler):
    def get(self):
        if UserInfo.is_current_user_admin() or UserInfo.is_current_user_super_admin():
            message = ''
            if self.request.get('action'):
                action = cgi.escape(self.request.get('action'))

                if action == 'v' or action == 'e':
                    message = cgi.escape(self.request.get('message'))

            self.response.write(globales.INDEX.render(user=UserInfo.greetings()))
            self.response.write(globales.UPLOAD.render(upload_url=blobstore.create_upload_url('/upload-blob-csv'),
                                                       message=message))
            self.response.write(globales.FOOTER.render(user=UserInfo.greetings()))
        else:
            self.redirect('/')

