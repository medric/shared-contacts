__author__ = 'medric'


from google.appengine.api import taskqueue
import cgi
import json
import webapp2
import csv
import datetime
import globales
import logging
import jsonpickle
import StringIO
from apiclient.http import MediaIoBaseUpload

from request import Request
from user_info import UserInfo
from models.service_account import ServiceAccount
from api_manager import ApiManager
from google.appengine.ext import deferred
from google.appengine.runtime import DeadlineExceededError

CSV = 'export_csv'


class ExportHandler(webapp2.RequestHandler):
    def get(self):
        action = cgi.escape(self.request.get('action'))

        if self.request.get('domain'):
            domain_id = cgi.escape(self.request.get('domain'))
            domain = ServiceAccount.get_domain_name(domain_id)
        else:
            domain = UserInfo.current_user().domain

        if domain:
            taskqueue.add(url='/export/run', params={'action': action,
                                                     'domain': domain,
                                                     'super_admin': UserInfo.is_current_user_super_admin()})
            self.redirect('/')


class Export(webapp2.RequestHandler):
    def post(self):
        action = cgi.escape(self.request.get('action'))
        domain = cgi.escape(self.request.get('domain'))
        service = ApiManager.get_api_service(domain)
        super_admin = cgi.escape(self.request.get('super_admin'))
        key = "contacts" + "_" + domain + "_" + datetime.date.today().strftime('%d - %b - %Y')
        feed_uri = service.GetFeedUri(
            contact_list=domain,
            projection='full'
        )
        if action == CSV:
            data = Export.retrieve(service, feed_uri)
            if data:
                Export.contacts_to_drive(key, data, super_admin, domain)

    @staticmethod
    def retrieve(service, feed_uri):
        try:
            out = []
            next_page = 1
            max_results = 700
            # Tant qu'il y a des contacts a recuperer
            while next_page:
                try:
                    data = Request.retrieve_shared_contacts(next_page, max_results, service, feed_uri)
                    if len(data['data']) > 0:
                        out += data['data']
                        next_page = data['next_page']
                    else:
                        next_page = None
                except Exception as e:
                    logging.error(e.message)
            return out
        except Exception as e:
            logging.error(e.message)

    @staticmethod
    def export_contacts_to_csv(entries, writer):
        writer.writerow(globales.CONTACTS_CSV)
        for entry in entries:
            entry = json.loads(entry)
            row = []
            row.append('')
            try:
                row.append(Request.get_contact_id(entry))
            except:
                row.append('')
            try:
                row.append(entry['email'][0]['address'].encode('utf-8'))
            except:
                row.append('')
            try:
                row.append(entry['name']['family_name']['text'].encode('utf-8'))
            except:
                row.append('')
            try:
                row.append(entry['name']['given_name']['text'].encode('utf-8'))
            except:
                row.append('')
            try:
                row.append(entry['organization']['title']['text'].encode('utf-8'))
            except:
                row.append('')
            try:
                row.append(entry['organization']['label']['text'].encode('utf-8'))
            except:
                row.append('')
            try:
                row.append(entry['phone_number'][0]['text'].encode('utf-8'))
            except:
                row.append('')
            try:
                row.append(entry['structured_postal_address'][0]['street']['text'].encode('utf-8'))
            except:
                row.append('')
            try:
                row.append(entry['structured_postal_address'][0]['city']['text'].encode('utf-8'))
            except:
                row.append('')
            try:
                row.append(entry['structured_postal_address'][0]['postcode']['text'].encode('utf-8'))
            except:
                row.append('')
            try:
                row.append(entry['structured_postal_address'][0]['region']['text'].encode('utf-8'))
            except:
                row.append('')
            try:
                row.append(entry['structured_postal_address'][0]['country']['text'].encode('utf-8'))
            except:
                row.append('')
            try:
                row.append(entry['link'][0]['href'].encode('utf-8'))
            except:
                row.append('')
            writer.writerow(row)

    @staticmethod
    def contacts_to_drive(key, entries, super_admin, domain):
        if super_admin == 'True':
            tmp = globales.SERVICE_ACCOUNT_GPARTNER
            service = ServiceAccount(
                domain=tmp.get('domain'),
                email_admin=tmp.get('email_admin'),
                pem_key=tmp.get('pem_key'),
                email_address=tmp.get('email_address')
            )
        else:
            service = ServiceAccount.get_by_domain(domain)
        csv = ''
        i = 0
        for value in globales.CONTACTS_CSV:
            csv += str(value)
            if i < len(globales.CONTACTS_CSV):
                csv += ','
            i += 1
        csv += '\n'
        for entry in entries:
            entry = json.loads(entry)
            csv += ','
            try:
                csv += Request.get_contact_id(entry) + ','
            except:
                csv += ','
            try:
                csv += entry['email'][0]['address'].encode('utf-8') + ','
            except:
                csv += ','
            try:
                csv += entry['name']['family_name']['text'].encode('utf-8') + ','
            except:
                csv += ','
            try:
                csv += entry['name']['given_name']['text'].encode('utf-8') + ','
            except:
                csv += ','
            try:
                csv += entry['organization']['title']['text'].encode('utf-8') + ','
            except:
                csv += ','
            try:
                csv += entry['organization']['label']['text'].encode('utf-8') + ','
            except:
                csv += ','
            try:
                csv += entry['phone_number'][0]['text'].encode('utf-8') + ','
            except:
                csv += ','
            try:
                csv += entry['structured_postal_address'][0]['street']['text'].encode('utf-8') + ','
            except:
                csv += ','
            try:
                csv += entry['structured_postal_address'][0]['city']['text'].encode('utf-8') + ','
            except:
                csv += ','
            try:
                csv += entry['structured_postal_address'][0]['postcode']['text'].encode('utf-8') + ','
            except:
                csv += ','
            try:
                csv += entry['structured_postal_address'][0]['region']['text'].encode('utf-8') + ','
            except:
                csv += ','
            try:
                csv += entry['structured_postal_address'][0]['country']['text'].encode('utf-8') + ','
            except:
                csv += ','
            try:
                csv += entry['link'][0]['href'].encode('utf-8')
            except:
                csv += ''
            csv += '\n'

        f = StringIO.StringIO(csv)

        media = MediaIoBaseUpload(f, mimetype='text/csv')

        body = {
            'title': key,
            'description': 'domain contacts',
            'mimeType': 'text/csv'
        }

        try:
            #insertion drive
            ApiManager.get_drive_service(service).files().insert(body=body, media_body=media, convert=True).execute()

            if file:
                file_id = file['id']
                new_permission = {
                    'value': UserInfo.current_user().email(),
                    'type': "user",
                    'role': "owner"
                }
                service.permissions().insert(fileId=file_id, sendNotificationEmails=True, body=new_permission).execute()
        except Exception as e:
            logging.error(e.message)