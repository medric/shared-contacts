__author__ = 'medric'

import cgi
import json

import webapp2

from request import Request
from user_info import UserInfo
from models.service_account import ServiceAccount
from api_manager import ApiManager


class Main(webapp2.RequestHandler):
    def post(self):
        data = None
        total = None
        action = cgi.escape(self.request.get('action'))
        contacts_dict = json.loads(self.request.body)

        if contacts_dict.get('domain'):
            domain_id = contacts_dict.get('domain')
            domain = ServiceAccount.get_domain_name(domain_id)
        else:
            domain = UserInfo.current_user().domain

        if domain:
            service = ApiManager.get_api_service(domain)

            feed_uri = service.GetFeedUri(
                contact_list=domain,
                projection='full'
            )

        if action == 'get_all':
            # Parametres de pagination
            limit = contacts_dict.get('limit')
            offset = contacts_dict.get('offset')
            data = Request.get_contacts(offset, limit, domain)
        elif action == 'search_contacts':
            # Parametres de pagination
            limit = contacts_dict.get('limit')
            offset = contacts_dict.get('offset')
            value = contacts_dict.get('value')
            data = Request.search_contacts(offset, limit, value, domain)
        elif action == 'insert_or_update':
            contact = contacts_dict.get('contact')
            data = Request.insert_or_update(contact, service, feed_uri, domain)
        elif action == 'delete_contacts':
            to_delete = contacts_dict.get('contacts')
            data = Request.delete_shared_contacts(to_delete, service, feed_uri, domain)
        elif action == 'get_app_users':
            data = Request.get_app_users(domain)
        elif action == 'delete_app_user':
            id = contacts_dict.get('id')
            data = Request.delete_app_user(id)
        elif action == 'update_app_user':
            id = contacts_dict.get('id')
            role = contacts_dict.get('role')
            data = Request.update_app_user(id, role)
        else:
            data = 'There is no action %s available, sorry' % action

        # Envoi de la reponse au FRONT (format json)
        if data:
            self.response.out.write(data)
        elif total:
            self.response.out.write(total)