__author__ = 'medric'

import webapp2
import cgi
from user_info import UserInfo
from models.service_account import ServiceAccount
import globales
from utils.encode import Encode


class ServiceAccountHandler(webapp2.RequestHandler):
    def get(self):
        if UserInfo.is_current_user_super_admin():
            message = ''
            if self.request.get('action'):
                action = cgi.escape(self.request.get('action'))
                if action == 'a':
                    try:
                        service_domain = cgi.escape(self.request.get('service-domain'))
                        service_address = cgi.escape(self.request.get('service-address'))
                        admin_address = cgi.escape(self.request.get('admin-address'))
                        pem_key = cgi.escape(self.request.get('pem-key'))

                        service = ServiceAccount(
                            id=Encode.encode_md5_hash(service_domain),
                            domain=service_domain,
                            email_admin=admin_address,
                            pem_key=pem_key,
                            email_address=service_address
                        )

                        service.insert_account()
                        self.redirect('/service-account?action=v&message=Done')
                    except Exception as e:
                        self.redirect('/service-account?action=e&message=Error')
                if action == 'v' or action == 'e':
                    message = cgi.escape(self.request.get('message'))

            self.response.write(globales.SERVICE_ACCOUNT_FORM.render(message=message))
        else:
            self.redirect('/')