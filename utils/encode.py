__author__ = 'medric'
import hashlib


class Encode:
    @staticmethod
    def encode_md5_hash(string):
        m = hashlib.md5()
        m.update(string)
        return m.hexdigest()