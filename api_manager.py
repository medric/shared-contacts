__author__ = 'medric'

import os
import sys
import inspect

cmd_subfolder = os.path.realpath(
    os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], "libs")))
if cmd_subfolder not in sys.path:
    sys.path.insert(0, cmd_subfolder)


#imports
import httplib2
from oauth2client.client import SignedJwtAssertionCredentials
from apiclient.discovery import build

import gdata.data
import gdata.contacts.client
import gdata.contacts.data
import gdata.gauth
from models.service_account import ServiceAccount


class ApiManager:
    def __init__(self):
        pass

    SCOPES = [
        'https://www.google.com/m8/feeds/'
    ]

    SCOPE_DRIVE_API = [
        'https://www.googleapis.com/auth/drive.file',
        'https://www.googleapis.com/auth/drive',
        'https://www.googleapis.com/auth/drive.appdata'
    ]

    @staticmethod
    def get_api_service(domain):
        try:
            service_account = ServiceAccount.get_by_domain(domain)
            gd_client = gdata.contacts.client.ContactsClient(domain)

            # Connection aux APIs
            credentials = SignedJwtAssertionCredentials(
                service_account_name=service_account.email_address,
                private_key=service_account.pem_key,
                scope=ApiManager.SCOPES,
                sub=service_account.email_admin
            )

            token = ApiManager.credentials_to_token(credentials)
            token.authorize(gd_client)

            return gd_client
        except Exception as e:
            return None

    @staticmethod
    def credentials_to_token(credentials):

        credentials.refresh(httplib2.Http())

        token = gdata.gauth.OAuth2Token(
            client_id=credentials.client_id,
            client_secret=credentials.client_secret,
            scope=credentials.scope,
            access_token=credentials.access_token,
            user_agent=credentials.user_agent,
            refresh_token=credentials.refresh_token
        )

        return token

    @staticmethod
    def get_drive_service(service):
        #connecting to the API
        credentials = SignedJwtAssertionCredentials(
            service_account_name=service.email_address,
            private_key=service.pem_key,
            scope=ApiManager.SCOPE_DRIVE_API,
            sub=service.email_admin
        )
        http = httplib2.Http()
        http = credentials.authorize(http)

        service_drive = build('drive', 'v2', http=http)

        return service_drive