# -*- coding: ISO-8859-1 -*-

__author__ = 'medric'

from api_manager import ApiManager
from models.app_user import AppUser
import time
import jsonpickle
import globales
from models.shared_contact import SharedContact
from models.address import Address
from utils.jsonEncode import ModelEncoder
from utils.encode import Encode


class Request:

    _json_encoder = ModelEncoder()

    def __init__(self):
        pass

    # Recherche des contacts partages sur les serveurs Google
    @staticmethod
    def retrieve_shared_contacts(start_index, max_results, service, feed_uri):
        cpt = 0
        go_on = True
        out = None

        # Back-off exponentiel
        while go_on:
            feed_uri += '?max-results=' + str(max_results) + '&start-index=' + str(start_index)

            try:
                data = service.get_contacts(uri=feed_uri).entry
                next_page = int(start_index) + int(max_results)
                # total_results = Request.get_total_results()

                out = {
                    'data': Request.format_response(data),
                    'next_page': next_page
                }

                go_on = False

            except Exception as e:
                if cpt < 5:
                    time.sleep(10 * cpt)
                    cpt += 1
                else:
                    raise Exception(e)

        return out

    @staticmethod
    def retrieve_shared_contacts_override(start_index, max_results, service, feed_uri):
        cpt = 0
        go_on = True
        out = None

        # Back-off exponentiel
        while go_on:
            feed_uri += '?max-results=' + str(max_results) + '&start-index=' + str(start_index)

            try:
                data = service.get_contacts(uri=feed_uri).entry
                next_page = int(start_index) + int(max_results)
                # total_results = Request.get_total_results()

                out = {
                    'data': data,
                    'next_page': next_page
                }

                go_on = False

            except Exception as e:
                if cpt < 5:
                    time.sleep(10 * cpt)
                    cpt += 1
                else:
                    raise Exception(e)

        return out

    # Recuperation des contacts depuis le datastore (necessite une execution de la tache "populate")
    @staticmethod
    def get_contacts(cursor, limit, domain):
        out = None

        try:
            # On recupere les contacts selon le domaine donn�
            out = SharedContact.get_contacts_paginate(cursor, limit, domain)
            out = Request._json_encoder.encode(out)
        except Exception as e:
            out = e.message

        return out

    @staticmethod
    def search_contacts(offset, limit, value, domain):
        outcome = None

        try:
            outcome = SharedContact.search_contacts(offset, limit, value, domain)
            outcome = Request._json_encoder.encode(outcome)
        except Exception as e:
            outcome = e.message

        return outcome

    @staticmethod
    def insert_or_update(contact_entry, service, feed_uri, domain):
        try:
            model = Request.make_model(contact_entry, domain)
            contact = SharedContact.get_contact_by_mail(contact_entry['mail'], domain)

            # Le contact n'existe pas dans le datastore, on l'ajoute
            if not contact:
                res = service.create_contact(model.to_contact_entry(), feed_uri)
                model.contactId = Request.get_contact_id(res)

            # Dans le cas contraire on le met a jour
            else:
                entry = Request.get_contact_by_id(contact.contactId, service, feed_uri)
                model.contactId = contact.contactId
                model.update_shared_contact(entry, service)

            model.save()

            outcome = {
                'message': 'success'
            }

            return Request._json_encoder.encode(outcome)
        except Exception as e:
            outcome = {
                'message': 'error',
                'exception': e.message
            }

            return Request._json_encoder.encode(outcome)

    @staticmethod
    def delete_shared_contacts(ids, service, feed_uri, domain):
        try:
            for id in ids:
                model = SharedContact.get_contact_by_id(id, domain)
                entry = Request.get_contact_by_id(model.contactId, service, feed_uri)
                if model:
                    # Suppression du datastore
                    model.delete()
                if entry:
                    # Suppression du contact des serveurs Google
                    service.Delete(entry)

            outcome = {
                'message': 'success'
            }

            return Request._json_encoder.encode(outcome)
        except Exception as e:
            outcome = {
                'message': 'error',
                'exception': e.message
            }

        return Request._json_encoder.encode(outcome)

    @staticmethod
    def make_model(contact_entry, domain):
        contact = SharedContact(id=Encode.encode_md5_hash(domain + str(contact_entry['mail'])))
        contact.contactId = ''
        contact.mail = contact_entry['mail']
        contact.firstName = contact_entry['firstName']
        contact.lastName = contact_entry['lastName']
        contact.phoneNumber = contact_entry['phoneNumber']
        contact.organization = contact_entry['organization']
        if contact_entry['organizationLabel'] in globales.ORGANIZATION_LABEL:
            contact.organizationLabel = contact_entry['organizationLabel']
        else:
            contact.organizationLabel = 'WORK'
        address = Address(
            street=contact_entry['address']['street'],
            city=contact_entry['address']['city'],
            postcode=contact_entry['address']['postcode'],
            region=contact_entry['address']['region'],
            country=contact_entry['address']['country']
        )
        contact.address = address
        contact.domain = domain

        return contact

    # Formate les donnees recuperees des serveurs google (contactEntry) en JSON
    @staticmethod
    def format_response(response):
        data = []

        for entry in response:
            data.append(jsonpickle.encode(entry))

        return data

    @staticmethod
    def get_contact_id(res):
        try:
            return res.id.text[res.id.text.find("base/")+5:len(res.id.text)]
        except Exception as e:
            return res['id']['text'][res['id']['text'].find("base/")+5:len(res['id']['text'])]

    @staticmethod
    def get_contact_by_id(id, service, feed_uri):
        try:
            uri = feed_uri + '/' + str(id)
            contact = service.get_contact(uri)

            if contact:
                return contact
            else:
                return None
        except Exception:
            return None

    @staticmethod
    def get_app_users(domain):
        try:
            out = AppUser.get_users_by_domain(domain)
        except Exception as e:
            out = e.message
        return Request._json_encoder.encode(out)

    @staticmethod
    def delete_app_user(id):
        try:
            AppUser.get_user_by_id(id).delete_user()
            outcome = {
                'message': 'success'
            }
        except Exception as e:
            outcome = {
                'message': 'error',
                'exception': e.message
            }
        return Request._json_encoder.encode(outcome)

    @staticmethod
    def update_app_user(id, role):
        try:
            if role in globales.USER_ROLES:
                user = AppUser.get_user_by_id(id)
                user.role = role
                user.insert_user()
                outcome = {
                    'message': 'success'
                }
            else:
                raise Exception('This role does not exist')
        except Exception as e:
            outcome = {
                'message': 'error',
                'exception': e.message
            }
        return Request._json_encoder.encode(outcome)


