#!/usr/bin/python
__author__ = 'medric'

import jinja2

jinja_environment = jinja2.Environment(autoescape=True, loader=jinja2.FileSystemLoader('html'))

# Templates HTML
INDEX = jinja_environment.get_template('index.html')
CONTENT = jinja_environment.get_template('content.html')
FOOTER = jinja_environment.get_template('footer.html')
UPLOAD = jinja_environment.get_template('upload.html')
LOGIN_ERROR_TEMPLATE = jinja_environment.get_template('login_error.html')
ACCESS_ERROR = jinja_environment.get_template('unauthorized_access.html')
SERVICE_ACCOUNT_FORM = jinja_environment.get_template('service_account.html')

APP_USERS_TEMPLATE = jinja_environment.get_template('users_render.html')

USER_ROLES = ['user', 'admin']
CSV_REPORT_HEADER = ['mail']
ORGANIZATION_LABEL = ['WORK', 'PERSONAL']
SUPER_DOMAIN = ""

MAIL_SENDER = ""

#CSV HEADER
CONTACTS_CSV = ['action', 'id', 'mail', 'lastName', 'firstName', 'organization', 'label', 'phoneNumber', 'street',
                'city', 'postcode', 'region', 'country', 'link']

#Service Account 
SERVICE_ACCOUNT = {
   'domain':'',
   'email_admin':'',
   'pem_key':file('', 'rb').read(),
   'email_address':''
}
