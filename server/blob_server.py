__author__ = 'medric'

import cgi
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers
from tasks.contacts_csv_manager import contactsCSVManager
from models.service_account import ServiceAccount
from user_info import UserInfo
from api_manager import ApiManager
from google.appengine.ext import deferred


def do_upload(blob, domain, user):
    service = ApiManager.get_api_service(domain)

    feed_uri = service.GetFeedUri(
        contact_list=domain,
        projection='full'
    )

    contactsCSVManager.contacts_csv_manager(service, user, feed_uri, domain, blob)


class UploadHandler(blobstore_handlers.BlobstoreUploadHandler):
    def post(self):
        upload_files = self.get_uploads('file')
        blob_info = upload_files[0]

        if cgi.escape(self.request.get('domain')) != '':
            domain_id = cgi.escape(self.request.get('domain'))
            domain = ServiceAccount.get_domain_name(domain_id)
        else:
            domain = UserInfo.current_user().domain

        if domain:
            if blob_info.filename.endswith('.csv'):
                try:
                    deferred.defer(do_upload, blobstore.BlobReader(blob_info).read(), domain
                                   , UserInfo.current_user().email, _queue="import")
                    self.redirect('/upload?action=v&message=Done')
                except Exception as e:
                    self.redirect('/upload?action=e&message=Error')
            else:
                self.redirect('/upload?action=e&message=File%20Error')
        else:
            self.redirect('/upload?action=e&message=Domain%20 Error')


# class DoUpload(webapp2.RequestHandler):
#     def post(self):
#         blob = self.request.get('blob')
#         domain = cgi.escape(self.request.get('domain'))
#         user = cgi.escape(self.request.get('user'))
#         service = ApiManager.get_api_service(domain)
#
#         feed_uri = service.GetFeedUri(
#             contact_list=domain,
#             projection='full'
#         )
#
#         Transaction.contacts_transaction(service, user, feed_uri, domain, blob)