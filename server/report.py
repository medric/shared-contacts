__author__ = 'medric'

import logging

from google.appengine.api import mail


class Report:
    @staticmethod
    def send_mail_error(sender, to, subject, data, date):
        try:
            list = ''.join(data)
            message = mail.EmailMessage(sender=sender,
                                        subject=subject)

            message.to = to
            message.subject = "Shared contacts update - " + date
            message.body = """

            Dear user,

            Errors occured during your shared contacts update.
            The CSV you have uploaded may be corrupted.

            These mails have not been treated (errors) :

            """ + list + """

            Please let us know if you have any questions.

            This is an automatic email, please don't answer.
            """

            message.send()
        except Exception as e:
            logging.error(e.message)

    @staticmethod
    def send_mail(sender, to, subject, date):
        try:
            message = mail.EmailMessage(sender=sender,
                                        subject=subject)

            message.to = to
            message.subject = "Shared contacts update - " + date
            message.body = """

            Dear user,

            Your domain shared contacts have been up to date.

            Please let us know if you have any questions.

            This is an automatic email, please don't answer.
            """

            message.send()
        except Exception as e:
            logging.error(e.message)
